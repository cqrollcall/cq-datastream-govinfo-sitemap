package com.cq.datastream;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.client.RestTemplate;

import com.cq.alex.api.v1.type.DocumentMeta;
import com.cq.alex.api.v1.type.DocumentSearchHit;
import com.cq.alex.api.v1.type.DocumentSearchRequest;
import com.cq.alex.api.v1.type.DocumentSearchResponse;

import oracle.jdbc.pool.OracleDataSource;

@SpringBootApplication
@EnableAutoConfiguration(exclude={JdbcTemplateAutoConfiguration.class, DataSourceAutoConfiguration.class})
@Configuration
public class CommitteeReportsRunner {
	
	private Logger log = LogManager.getLogger(CommandLineRunner.class);
	
	@Autowired
	private Environment env;
	
	private JdbcTemplate readTemplate;
	
	private JdbcTemplate writeTemplate;
	
	private String table;
	
	private final String APPROPRIATIONS_SQL="select p.ft_group_code as code, p.full_name as label from CQAPPS.pubgroup p, CQAPPS.product_pubgroup pp, cqapps.product_cycle_ref pc where p.pubgroup_id = pp.pubgroup_id and pp.product_type_id = 4 and p.product_cycle_type_id = pc.product_cycle_type_id and pc.ft_product_cycle_code = 'FY21' order by p.full_name";
		
	public static void main(String[] args) {
		new SpringApplicationBuilder(CommitteeReportsRunner.class).web(WebApplicationType.NONE).build().run(args);
	}  
	

	@Bean
	public void readTemplate() throws SQLException{
		log.info("Login to Read DB with user "+env.getProperty("read.jdbc.username"));
		OracleDataSource dataSource = new OracleDataSource();
		dataSource.setURL(env.getProperty("read.jdbc.url"));
		dataSource.setUser(env.getProperty("read.jdbc.username"));
		dataSource.setPassword(env.getProperty("read.jdbc.password"));

		readTemplate = new JdbcTemplate(dataSource);
	} 
	

	@Bean
	public void writeTemplate() throws SQLException{
		log.info("Login to Write DB with user "+env.getProperty("write.jdbc.username"));
		OracleDataSource dataSource = new OracleDataSource();
		dataSource.setURL(env.getProperty("write.jdbc.url"));
		dataSource.setUser(env.getProperty("write.jdbc.username"));
		dataSource.setPassword(env.getProperty("write.jdbc.password"));

		writeTemplate = new JdbcTemplate(dataSource);
	}   
	  
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		log.info("Login to Alex with user "+env.getProperty("alex.login.username"));
		builder.basicAuthentication(env.getProperty("alex.login.username"), env.getProperty("alex.login.password"));
		RestTemplate restTemplate = builder.build();
		
		List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();
		
		if(interceptors == null)
			interceptors = new ArrayList<ClientHttpRequestInterceptor>();

		interceptors.add(new ClientHttpRequestInterceptor() {
			
			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
				HttpHeaders currHeaders = request.getHeaders();
				currHeaders.add("cqrcUserId", env.getProperty("alex.api.userid"));
				return execution.execute(request, body);
			}
		});
		restTemplate.setInterceptors(interceptors);
		
		return restTemplate;
	}
	
	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
			try {
				String apiBase = env.getProperty("alex.api.base");				
				
				table = env.getProperty("target.table");
				if(table == null || table.equals("")){
					SimpleDateFormat df = new SimpleDateFormat("YYYYMMdd");
					table = "TMP_LT_COMM_RPT_"+df.format(new Date());
					
					table = env.getProperty("write.jdbc.username") + "." + table;
					table = table + "_" + env.getActiveProfiles()[0];
				}
				
				boolean createTable = false;
				
				try{
					writeTemplate.query("select * from " + table, new RowMapper<String>(){

						@Override
						public String mapRow(ResultSet rs, int rowNum) throws SQLException {
							// TODO Auto-generated method stub
							return null;
						}
						
					});
				}catch(Exception e){
					log.info("Table doesn't exist");
					createTable= true;
				}
				
				log.info("Table ===> " + table + ",createTable ==> " + createTable);
				
				if(createTable){
					String tableCreationScript = "CREATE TABLE " + table
							+ " (DOC_UID VARCHAR2(50 BYTE), "
							+ "BILL_NUMBER CLOB, "
							+ "HEADLINE VARCHAR2(2000 BYTE), "
							+ "PUB_DATE DATE, "
							+ "APPROPRIATION VARCHAR2(200 BYTE), "
							+ "COMMITTEE_TYPE VARCHAR2(100 BYTE), "
							+ "COMMITTEE VARCHAR2(500 BYTE), "
							+ "FISCAL_YEAR VARCHAR2(10 BYTE), "
							+ "DISPLAY_REPORT_NUMBER VARCHAR2(500 BYTE), "
							+ "WORD_COUNT NUMBER(10,0), "
							+ "COMMITTEE_REPORT_NUMBER VARCHAR2(512 BYTE))";
					
					writeTemplate.execute(tableCreationScript);
				}
				
//				String threadSizeStr = env.getProperty("runner.thread.size");
//				Integer threadSize = Integer.parseInt(threadSizeStr);
				
				// Appropriations
				List<Appropriation> appropsList = readTemplate.query(APPROPRIATIONS_SQL, new RowMapper<Appropriation>(){

					@Override
					public Appropriation mapRow(ResultSet rs, int rowNum) throws SQLException {
						Appropriation app = new Appropriation();
						app.setCode(rs.getString("CODE"));
						app.setLabel(rs.getString("LABEL"));
						return app;
					}
					
				});
				
				
				for (Iterator iterator = appropsList.iterator(); iterator.hasNext();) {
					Appropriation appropriation = (Appropriation) iterator.next();
					
					log.info("Running Appropriation Entries: " + appropriation.getLabel());
					
					String cqql = prepareCQQL("approps.cqql", new HashMap<String,String>(){
						{
							put("CQ_CONGRESS_NUMBER","116");
							put("CQ_BILL_GROUP", appropriation.getCode());
						}
					});
					
					extractAndInsert(restTemplate, apiBase, table, cqql, appropriation.getLabel(), null, null, null);
					
				}
				
				// Committee Type
				for (CommitteeType type : CommitteeType.values()) {
					
					log.info("Running Committee Type Entries: " + type.getLabel());
					
					String cqql = prepareCQQL("committee_type.cqql", new HashMap<String,String>(){
						{
							put("CQ_CONGRESS_NUMBER","116");
							put("CQ_COMMITTEE_TYPE", type.name());
						}
					});
					
					extractAndInsert(restTemplate, apiBase, table, cqql, null, type.getLabel(), null, null);
					
				}
				
				// Committees
				for (Committee type : Committee.values()) {
					
					log.info("Running Committee Entries: " + type.getLabel());
					
					String cqql = prepareCQQL("commitees.cqql", new HashMap<String,String>(){
						{
							put("CQ_CONGRESS_NUMBER","116");
							put("CQ_COMMITTEE", type.name());
						}
					});
					
					extractAndInsert(restTemplate, apiBase, table, cqql, null, null, type.getLabel(), null);
					
				}
				
				// Sub Committees
				for (SubCommittee type : SubCommittee.values()) {
					
					log.info("Running Sub Committee Entries: " + type.getLabel());
					
					String cqql = prepareCQQL("commitees.cqql", new HashMap<String,String>(){
						{
							put("CQ_CONGRESS_NUMBER","116");
							put("CQ_COMMITTEE", type.name());
						}
					});
					
					extractAndInsert(restTemplate, apiBase, table, cqql, null, null, type.getLabel(), null);
					
				}
				
				// Sub Committees
				
				for (int fy = 3; fy < 21; fy++) {

					String fiscalYear = "FY" + String.format("%02d", fy);
					
					log.info("Running Fiscal Year Entries: " + fiscalYear);
					
					String cqql = prepareCQQL("fiscal_year.cqql", new HashMap<String,String>(){
						{
							put("CQ_CONGRESS_NUMBER","116");
							put("CQ_FISCAL_YEAR", fiscalYear);
						}
					});
					
					extractAndInsert(restTemplate, apiBase, table, cqql, null, null, null, fiscalYear);
					
				}
				
			} catch (Exception e) {
				log.error("Failed to run runner",e);
				System.exit(1);
			}
			System.exit(0);
		};
	}


	private void extractAndInsert(RestTemplate restTemplate, String apiBase, String table, String cqql, String approps, String commType, String committee, String fiscalYear) {
		DocumentSearchRequest criteria = new DocumentSearchRequest();
		criteria.setCqql(cqql);
		criteria.setCount(100000);
		criteria.setOffset(0);
		try{
			DocumentSearchResponse response = restTemplate.postForObject(apiBase + "docs" , criteria, DocumentSearchResponse.class);

			log.info("Total Found: " + response.getTotalFound());
			
			List<DocumentSearchHit> hits = response.getHits();
			for (DocumentSearchHit hit : hits) {
				DocumentMeta meta = hit.getMeta();  
				// Insert into table  DOC_UID
				
				log.info("Doc UID: " + meta.getUid());
				
				writeTemplate.update("insert into " + table + " (DOC_UID, "
						+ "BILL_NUMBER, "
						+ "HEADLINE, "
						+ "PUB_DATE, "
						+ "APPROPRIATION, "
						+ "COMMITTEE_TYPE, "
						+ "COMMITTEE, "
						+ "FISCAL_YEAR, "
						+ "DISPLAY_REPORT_NUMBER, "
						+ "WORD_COUNT, "
						+ "COMMITTEE_REPORT_NUMBER) values (?,?,?,?,?,?,?,?,?,?,?)",
						meta.getUid(),
						meta.getBillNums() != null ? String.join(",", meta.getBillNums()) : "",
						meta.getHeadline(),
						meta.getPublicationDate(),
						approps,
						commType,
						committee,
						fiscalYear,
						meta.getDisplayReportNum(),
						meta.getWordCount(),
						(String.join("", meta.getChamber(), meta.getReportNum(), meta.getPartNum())));
			}
		}catch(Exception e){
			log.error("Document fetch failed for cqql: [" + cqql + "], with error: ", e);
//			System.exit(1);
		}
		
	}
	
	private String prepareCQQL(String cqqlProperty, HashMap<String, String> replacers) {
		String cqql = env.getProperty(cqqlProperty);
		
		for (Entry<String, String> replacer : replacers.entrySet()) {
			cqql = cqql.replace(replacer.getKey(), replacer.getValue());
		}

		return cqql;
	}

	enum CommitteeType{
		HHSE("House"), SSEN("Senate"), JJNT("Joint"), CCNF("Conference");
		
		private String label;
		
		private CommitteeType(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return label;
		}
	}
	
	enum Committee{
		HBAN("House Financial Services"),SBUD("Senate Budget"),CONF("Conference Committee");
		
		private String label;
		
		private Committee(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return label;
		}
	}
	
	enum SubCommittee{
		HRAM05("Oversight & Investgations"),HBAN30("Financial Institutions & Consumer Credit"),HBAN50("Housing and Insurance"), HBAN60("Monetary Policy & Trade"), HBAN40("Oversight & Investigations"), HBAN70("Terrerism Financing Task Force"), HFIN10("Capital Markets..");
		
		private String label;
		
		private SubCommittee(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return label;
		}
	}
	
	class Appropriation{
		String code;
		String label;
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getLabel() {
			return label;
			
			
		}
		public void setLabel(String label) {
			this.label = label;
		}
		
	}
}
